import 'dart:io';

import 'package:csv/csv.dart';
import 'package:simulador_vek/models/db_entities/client.model.dart';
import 'package:simulador_vek/models/db_entities/submit.model.dart';
import 'package:simulador_vek/models/db_entities/taxes.model.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

class GenerateViewModel extends ChangeNotifier {

  Box simulationBox;
  ClientModel cliente;
  TaxModel taxModel;

  add(OnSubmit newSimulation) {
    Hive.box('Submited').add(newSimulation);
    notifyListeners();
  }

  getAll() {
    return Hive.box('Submited').values.toList();
  }

  bool taxasLimiteChecker() {
    
    return taxasDebitoAbaixoLimite() && taxasCreditoAbaixoLimite();
  }

  Map taxasMinimasPorAtividade() {
  
    Map taxes = {
      'Ramo de Atividade1': {"taxa_debito": 10.2, "taxa_credito": 3.4},
      'Ramo de Atividade2': {"taxa_debito": 3.8, "taxa_credito": 2.6},
      'Ramo de Atividade3': {"taxa_debito": 7.5, "taxa_credito": 6.2},
    };

    return taxes[cliente.ramoAtividade];
  }

  bool taxasDebitoAbaixoLimite() {
    Map taxes = taxasMinimasPorAtividade();
    return taxes["taxa_debito"] < taxModel.taxaDebitoAposDesconto;
  }

  bool taxasCreditoAbaixoLimite() {
    Map taxes = taxasMinimasPorAtividade();
    return taxes["taxa_credito"] < taxModel.taxaCreditoAposDesconto;
  }

  generateCsv() {
    List<List<dynamic>> rows = List<List<dynamic>>();

    // titles of rows
    List<dynamic> row = List();

    [
      'concorrente',
      'cpf',
      'email',
      'ramoAtividade',
      'telefone',
      'taxaDebito',
      'taxaCredito',
      'descontoDebito',
      'descontoCredito',
      'debito_taxa_final_aceita',
      'credito_taxa_final_aceita',
      'dataAceite'
    ].forEach((v) => row.add(v));
    rows.add(row);

    Hive.box('Submited').values.forEach((simulation) {
      if (simulation.accepted == false) return null;

      List<dynamic> row = List();
      row.add(simulation.taxModel.concorrente);

      row.add(simulation.cliente.cpf);
      row.add(simulation.cliente.email);
      row.add(simulation.cliente.ramoAtividade);
      row.add(simulation.cliente.telefone);

      row.add(simulation.taxModel.taxaDebito);
      row.add(simulation.taxModel.taxaCredito);

      row.add(simulation.taxModel.descontoDebito);
      row.add(simulation.taxModel.desconto_credito);

      row.add(simulation.taxModel.taxaDebitoAposDesconto);
      row.add(simulation.taxModel.taxaCreditoAposDesconto);

      row.add(simulation.dataAceite);
      rows.add(row);
    });

    return rows;
  }

  abrirCsv(BuildContext context) async {
    if (!kIsWeb) {
      final permissionValidator = EasyPermissionValidator(
        context: context,
        appName: 'Permissão para emissão do CSV',
      );

      var result = await permissionValidator.storage();

      if (result) {
        String dir = (await getExternalStorageDirectory()).absolute.path +
            "/arquivoProposta.csv";
        var file = "$dir";
        File f = new File(file);

        String csv = const ListToCsvConverter().convert(generateCsv());
        await f.writeAsString(csv);

        await OpenFile.open(file); 

      }
    } else throw Exception("Cliente WEB não pode gerar e carregar arquivos.");
  }

  concorrentesLista() {
    return ['Concorrente 1', 'Concorrente 2', 'Concorrente 3', 'Concorrente 4'];
  }

  atividadesLista() {
    return ['1', '2', '3'].map((v) => "Ramo de Atividade" + v).toList();
  }
}
