import 'package:flutter/material.dart';

class MyHintText extends StatelessWidget {
  
  String title;
  String description;

  MyHintText({this.title, this.description});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 32.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          this.title,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
        ),
        Text(
          this.description,
          style: TextStyle(fontSize: 10),
        ),
      ]),
    );
  }
}
