import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyDropdownField extends StatefulWidget {

  String hint;
  List<String> elementos;
  Function(String) onSaved;
  Function(String) validator;
  
  MyDropdownField(
      {this.hint, this.elementos, this.onSaved, this.validator});

  @override
  _MyDropdownFieldState createState() => _MyDropdownFieldState();
}

class _MyDropdownFieldState extends State<MyDropdownField> {
  String selected;

  _dropdownfunction() {
    return FormField<String>(
      validator: (_) { return widget.validator(this.selected);},
      onSaved: (_) => widget.onSaved(this.selected),
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: InputDecoration(
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 1),
              ),
              contentPadding: EdgeInsets.all(3.0),
              border: const OutlineInputBorder(),
              fillColor: Colors.white,
            ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: Text("Selecione"),
                  value: selected,
                  onChanged: (String newValue) {
//                    state.didChange(newValue);
                    setState(() {
                      selected = newValue;
                    });
                  },
                  items: widget.elementos.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
            ),
//            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
//         return Padding(
//       padding: EdgeInsets.all(8.0),
//       child: FormField<String>(
//         builder: (FormFieldState<String> state) {
//           return InputDecorator(
//             decoration: InputDecoration(
//               enabledBorder: const OutlineInputBorder(
//                 borderSide: const BorderSide(color: Colors.grey, width: 1),
//               ),
//               contentPadding: EdgeInsets.all(15.0),
//               border: const OutlineInputBorder(),
//               fillColor: Colors.white,
//             ),                     
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               children: <Widget>[
//                 new DropdownButtonHideUnderline(
//                 child: DropdownButton<String>(
//                   hint: Text("Selecione"),
//                   value: selected,
//                   onChanged: (String newValue) {
// //                    state.didChange(newValue);
//                     setState(() {
//                       selected = newValue;
//                     });
//                   },
//                   items: widget.listElements.map((String value) {
//                     return DropdownMenuItem<String>(
//                       value: value,
//                       child: Text(value),
//                     );
//                   }).toList(),
//                 ),
//               ),
//               ],
//             ),
//           );
//         },
//       ),
//     );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _dropdownfunction();
  }
}
