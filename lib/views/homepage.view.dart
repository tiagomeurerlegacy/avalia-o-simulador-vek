import 'package:simulador_vek/vm/generate.vm.dart';
import 'package:simulador_vek/views/csv.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var modelView = Provider.of<GenerateViewModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Simulação", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey[200],
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Form(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(20),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.grey[600],
                    border: Border.all(width: 1, color: Colors.grey[600]),
                  ),
                  child: FlatButton(
                    child: Text(
                      "Nova Simulação",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                    onPressed: () =>
                        Navigator.pushNamed(context, "/clientFormRoute"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(20),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(width: 2, color: Colors.grey[600]),
                  ),
                  child: FlatButton(
                    child: Text(
                      "Visualizar propostas aceitas",
                      style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 19,
                      ),
                    ),
                    onPressed: () {
                      if (!kIsWeb) return modelView.abrirCsv(context);
                      var csv = modelView.generateCsv();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => CsvFile(
                                    data: csv,
                                  )));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
