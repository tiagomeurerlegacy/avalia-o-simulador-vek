import 'package:flutter/material.dart';


class CsvFile extends StatelessWidget {
  List<List> data;

  CsvFile({this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Table Layout and CSV"),
      ),
      body: SingleChildScrollView(
          child: Table(
        columnWidths: {
          0: FixedColumnWidth(100.0),
          1: FixedColumnWidth(200.0),
        },
        border: TableBorder.all(width: 1.0),
        children: data != null
            ? _table()
            : Center(
                child: Text("Empty Table"),
              ),
      )),
    );
  }

  _table() {
    return data.map((item) {
      return TableRow(
          children: item.map((row) {
        return Text(row.toString());
      }).toList());
    }).toList();
  }
}
