import 'package:simulador_vek/models/db_entities/client.model.dart';
import 'package:simulador_vek/vm/generate.vm.dart';
import 'package:simulador_vek/utils/dropdown.util.dart';
import 'package:simulador_vek/utils/textfield.util.dart';
import 'package:simulador_vek/utils/hint.util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:provider/provider.dart';

class ClientFormView extends StatelessWidget {

  GlobalKey<FormState> _validatorFormKey = GlobalKey<FormState>();

  Widget _buildForm(BuildContext context) {

    GenerateViewModel modelView = Provider.of<GenerateViewModel>(context);

    return Form(
      key: _validatorFormKey,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: MyTextField(
                    hint: 'CPF *',
                    controller: MaskedTextController(
                      mask: '000.000.000-00',
                    ),
                    onSaved: (value) => modelView.cliente.cpf = value,
                    textInputType: TextInputType.number,
                    validator: (value) {
                      if (value.length < 14) return "CPF inválido";
                    }),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: MyTextField(
                    hint: 'Telefone *',
                    controller: MaskedTextController(
                      mask: '(00) 0 0000-0000',
                    ),
                    textInputType: TextInputType.phone,
                    onSaved: (value) => modelView.cliente.telefone = value,
                    validator: (value) {
                      if (value.length < 16) return "Telefone inválido";
                    }),
              ),
            ],
          ),
          MyTextField(
            hint: 'Email',
            onSaved: (value) => modelView.cliente.email = value,
            textInputType: TextInputType.emailAddress,
          ),
          SizedBox(height: 20),
          MyDropdownField(
            validator: (value) {
              if (value == null) return "Selecione o ramo de atividade";
              return null;
            },
            onSaved: (value) {
              modelView.cliente.ramoAtividade = value;
            },
            hint: "Ramo de atividade *",
            elementos: modelView.atividadesLista(),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildbottomButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text("Simulação", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey[200],
        actions: <Widget>[],
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Dados do Cliente",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Os campos obrigatórios estão sinalizados com *",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  _buildForm(context)
                ]),
          ),
        ],
      ),
    );
  }

  _buildbottomButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 200.0),
      height: 60,
      width: 300,
      decoration: BoxDecoration(
        color: Colors.grey[700],
      ),
      child: FlatButton(
        child: Text(
          "Próximo     >",
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
          ),
        ),
        onPressed: () => _buildNavigation(context),
      ),
    );
  }

  _buildNavigation(BuildContext context) {

    if (_validatorFormKey.currentState.validate()) {
      Provider.of<GenerateViewModel>(context).cliente =
          ClientModel('', '', '', '');
      _validatorFormKey.currentState.save();
      Navigator.pushNamed(context, '/taxFormRoute');
    }

  }
}
