import 'package:simulador_vek/models/db_entities/taxes.model.dart';
import 'package:simulador_vek/vm/generate.vm.dart';
import 'package:simulador_vek/utils/dropdown.util.dart';
import 'package:simulador_vek/utils/textfield.util.dart';
import 'package:simulador_vek/utils/hint.util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class TaxFormView extends StatelessWidget {

  GlobalKey<FormState> _validatorFormKey = GlobalKey<FormState>();

  Widget _buildForm(BuildContext context) {

    GenerateViewModel modelView = Provider.of<GenerateViewModel>(context);

    return Form(
      key: _validatorFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MyDropdownField(
            validator: (value) {
              if (value == null) return "Selecione o concorrente";
            },
            onSaved: (value) {
              modelView.taxModel.concorrente = value;
            },
            hint: "Concorrente *",
            elementos: modelView.concorrentesLista(),
          ),
          Text(
            "Débito",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: MyTextField(
                  hint: 'Taxa do concorrente *',
                  textInputType: TextInputType.number,
                  onSaved: (value) =>
                      modelView.taxModel.taxaDebito = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    return null;
                  },
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: MyTextField(
                  hint: 'Desconto oferecido *',
                  textInputType: TextInputType.number,
                  onSaved: (value) =>
                      modelView.taxModel.descontoDebito = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    if (double.parse(value) > 100.0)
                      return "desconto não pode ser maior que 100%";
                    return null;
                  },
                ),
              ),
            ],
          ),
          Text(
            "Crédito",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: MyTextField(
                  hint: 'Taxa do concorrente *',
                  textInputType: TextInputType.number,
                  onSaved: (value) =>
                      modelView.taxModel.taxaCredito = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    return null;
                  },
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: MyTextField(
                  hint: 'Desconto oferecido *',
                  textInputType: TextInputType.number,
                  onSaved: (value) =>
                      modelView.taxModel.descontoCredito = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    if (double.parse(value) > 100.0)
                      return "desconto não pode ser maior que 100%";
                    return null;
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildbottomButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text("Simulação", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey[200],
        actions: <Widget>[],
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Informações de Taxas",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Os campos obrigatórios estão sinalizados com *",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  _buildForm(context),
                ]),
          ),
        ],
      ),
    );
  }

  _buildbottomButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 200.0),
      height: 60,
      width: 300,
      decoration: BoxDecoration(
        color: Colors.grey[700],
      ),
      child: FlatButton(
        child: Text(
          "Próximo     >",
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
          ),
        ),
        onPressed: () => _submitSimulation(context),
      ),
    );
  }

  _submitSimulation(BuildContext context) {

    GenerateViewModel modelView = Provider.of<GenerateViewModel>(context);

    if (_validatorFormKey.currentState.validate()) {
      modelView.taxModel = TaxModel("", 0.0, 0.0, 0.0, 0.0);
      _validatorFormKey.currentState.save();

      Navigator.pushNamed(context, '/submitRoute');
    }
  }
}
