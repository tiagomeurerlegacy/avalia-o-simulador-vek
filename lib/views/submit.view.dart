import 'package:simulador_vek/models/db_entities/submit.model.dart';
import 'package:simulador_vek/vm/generate.vm.dart';
import 'package:simulador_vek/utils/hint.util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class SubmitFormView extends StatelessWidget {

  _buildDataTable(BuildContext context) {

    GenerateViewModel simulationModelView = Provider.of<GenerateViewModel>(context);

    return DataTable(
      columns: [
        DataColumn(label: Text('Tipo')),
        DataColumn(label: Text('Concorrente')),
        DataColumn(label: Text('Proposta')),
      ],
      rows: [
        DataRow(cells: [
          DataCell(Text('Débito')),
          DataCell(
            Text("${simulationModelView.taxModel.taxaDebito.toString()}%"),
          ),
          DataCell(
            Text(
              "${simulationModelView.taxModel.taxaDebitoAposDesconto.toStringAsFixed(2)}%",
              style: simulationModelView.taxasDebitoAbaixoLimite()
                  ? TextStyle(color: Colors.green)
                  : TextStyle(color: Colors.red),
            ),
          ),
        ]),
        DataRow(cells: [
          DataCell(Text('Crédito')),
          DataCell(
            Text("${simulationModelView.taxModel.taxaCredito.toString()}%"),
          ),
          DataCell(
            Text(
              "${simulationModelView.taxModel.taxaCreditoAposDesconto.toStringAsFixed(2)}%",
              style: simulationModelView.taxasCreditoAbaixoLimite()
                  ? TextStyle(color: Colors.green)
                  : TextStyle(color: Colors.red),
            ),
          ),
        ]),
      ],
    );
  }

  _buildTaxasPermitidas(BuildContext context) {

    return Scaffold(
      bottomNavigationBar: _buildbottomButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text("Simulador", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey[200],
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Taxas da Simulação Permitidas",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Todas as taxas simuladas são permitidas",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Ofereça ao seu cliente!",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                  _buildDataTable(context),
                ]),
          ),
        ],
      ),
    );
  }

  _buildTaxasNaoPermitidas(BuildContext context) {
    GenerateViewModel simulationModelView =
        Provider.of<GenerateViewModel>(context);
    return Scaffold(
      bottomNavigationBar: _buildbottomButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text("Simulação", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey[200],
        actions: <Widget>[],
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  MyHintText(
                      title: 'Taxas da simulação não permitidas',
                      description: 'A(s) taxa(s) simulada(s) não foi(ram) '
                          'permitidas. Pois, não atinge(m) o mínimo requisitado'),
                  _buildDataTable(context),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: Text(
                      "O valor da taxa mínima para débito é "
                      "${simulationModelView.taxasMinimasPorAtividade()['taxa_debito']}%"
                      " enquanto para crédito é "
                      "${simulationModelView.taxasMinimasPorAtividade()['taxa_credito']}% ",
                      style: TextStyle(fontSize: 10),
                    ),
                  )
                ]),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    GenerateViewModel modelView = Provider.of<GenerateViewModel>(context);
    if (modelView.taxasLimiteChecker())
      return _buildTaxasPermitidas(context);
    else
      return _buildTaxasNaoPermitidas(context);
      
  }

  _buildbottomButton(BuildContext context) {
    GenerateViewModel simulationModelView =
        Provider.of<GenerateViewModel>(context);

    if (!simulationModelView.taxasLimiteChecker())
      return Container(
        margin: EdgeInsets.only(top: 200.0),
        height: 60,
        width: 300,
        decoration: BoxDecoration(
          color: Colors.grey[700],
        ),
        child: FlatButton(
          child: Text(
            "Reajustar",
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      );

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 60,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.grey[700],
                ),
                child: FlatButton(
                  child: Text(
                    "Proposta aceita",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                    ),
                  ),
                  onPressed: () {
                    simulationModelView.add(OnSubmit(simulationModelView.cliente,
                        simulationModelView.taxModel, true, DateTime.now()));
                    _buildDialog(context);
                  },
                ),
              ),
              Container(
                height: 60,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.grey[100],
                ),
                child: FlatButton(
                  child: Text(
                    "Recusar",
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 25,
                    ),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future _buildDialog(BuildContext context) {
    return showDialog<String>(
        context: context,
        barrierDismissible: false,
        // dialog is dismissible with a tap on the barrier
        builder: (BuildContext context) {
          return AlertDialog(
              title: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Proposta Salva"),
              SizedBox(height: 5),
              Text(
                "Proposta foi gravada no banco de dados.\n",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
              ),

              // make buttons use the appropriate styles for cards
              RaisedButton(
                onPressed: () =>
                    Navigator.of(context).popUntil((route) => route.isFirst),
                color: Colors.black54,
                child:
                    Text("Menu Inicial", style: TextStyle(color: Colors.white)),
              ),
            ],
          ));
        });
  }
}
