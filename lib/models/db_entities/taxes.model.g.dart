// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'taxes.model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TaxModelAdapter extends TypeAdapter<TaxModel> {
  @override
  TaxModel read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TaxModel(
      fields[0] as String,
      fields[2] as double,
      fields[4] as double,
      fields[1] as double,
      fields[3] as double,
    );
  }

  @override
  void write(BinaryWriter writer, TaxModel obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.concorrente)
      ..writeByte(1)
      ..write(obj.taxaDebito)
      ..writeByte(2)
      ..write(obj.taxaCredito)
      ..writeByte(3)
      ..write(obj.descontoDebito)
      ..writeByte(4)
      ..write(obj.descontoCredito);
  }
}
