import 'package:simulador_vek/models/db_entities/taxes.model.dart';
import 'package:hive/hive.dart';


import 'client.model.dart';

part 'submit.model.g.dart';

@HiveType()

class OnSubmit extends HiveObject {

  @HiveField(0)
  ClientModel client;
  @HiveField(1)
  TaxModel taxModel;
  @HiveField(2)
  bool accepted;
  @HiveField(3)
  DateTime dataAceite;

  OnSubmit(
      this.client, this.taxModel, this.accepted, this.dataAceite);


}
