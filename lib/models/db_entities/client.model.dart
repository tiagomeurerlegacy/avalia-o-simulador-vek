import 'package:hive/hive.dart';

part 'client.model.g.dart';

@HiveType()
class ClientModel extends HiveObject {

  @HiveField(0)
  String cpf;
  @HiveField(1)
  String telefone;
  @HiveField(2)
  String email;
  @HiveField(3)
  String ramoAtividade;

  ClientModel(this.cpf, this.telefone, this.email, this.ramoAtividade);
}
