// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'submit.model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OnSubmitAdapter extends TypeAdapter<OnSubmit> {
  @override
  OnSubmit read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OnSubmit(
      fields[0] as ClientModel,
      fields[1] as TaxModel,
      fields[2] as bool,
      fields[3] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, OnSubmit obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.client)
      ..writeByte(1)
      ..write(obj.taxModel)
      ..writeByte(2)
      ..write(obj.accepted)
      ..writeByte(3)
      ..write(obj.dataAceite);
  }
}
