import 'package:hive/hive.dart';

part 'taxes.model.g.dart';

@HiveType()

class TaxModel extends HiveObject {

  @HiveField(0)
  String concorrente;
  @HiveField(1)
  double taxaDebito;
  @HiveField(2)
  double taxaCredito;
  @HiveField(3)
  double descontoDebito;
  @HiveField(4)
  double descontoCredito;

  TaxModel(
      this.concorrente,
      this.taxaCredito,
      this.descontoCredito,
      this.taxaDebito,
      this.descontoDebito);


  double get taxaCreditoAposDesconto {
    return this.taxaCredito -
        this.taxaCredito *
            this.descontoCredito /
            100;
  }

  double get taxaDebitoAposDesconto {
    return this.taxaDebito -
        this.taxaDebito *
            this.descontoDebito /
            100;
  }
}
