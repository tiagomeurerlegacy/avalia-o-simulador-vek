import 'package:simulador_vek/models/db_entities/submit.model.dart';
import 'package:simulador_vek/models/db_entities/client.model.dart';
import 'package:simulador_vek/models/db_entities/taxes.model.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

import 'package:path_provider/path_provider.dart';

initHive() async {
  if (!kIsWeb) {
    final appDocumentDir = await getApplicationDocumentsDirectory();
    Hive.init(appDocumentDir.path);
  }

  Hive.registerAdapter(ClientModelAdapter(), 0);
  Hive.registerAdapter(TaxModelAdapter(), 1);
  Hive.registerAdapter(OnSubmitAdapter(), 2);

  await Hive.openBox('Cliente');
  await Hive.openBox('TaxModel');
  return await Hive.openBox('Submited');
}
