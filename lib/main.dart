import 'package:simulador_vek/models/db.model.dart';
import 'package:simulador_vek/vm/generate.vm.dart';
import 'package:simulador_vek/views/homepage.view.dart';
import 'package:simulador_vek/views/submit.view.dart';
import 'package:simulador_vek/views/clientform.view.dart';
import 'package:simulador_vek/views/taxform.view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

main() async {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _openDb();
  }

  _openDb() {
    return FutureBuilder(
      future: initHive(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => GenerateViewModel()),
            ],
            child: MaterialApp(
              title: 'Simulação',
              theme: ThemeData(
                primarySwatch: Colors.grey,
              ),
              routes: {
                '/clientFormRoute': (context) => ClientFormView(),
                '/taxFormRoute': (context) => TaxFormView(),
                '/submitRoute': (context) => SubmitFormView(),
              },
              home: snapshot.error == null ? HomePage() : _errorScreen(),
            ),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  _errorScreen() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Erro", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey[200],
        actions: <Widget>[],
      ),
      body: Center(
        child: Text('Erro ao carregar a aplicação!'),
      ),
    );
  }
}
